package com.allu.spring;

import com.allu.spring.entities.Student;
import com.allu.spring.repositories.CourseRepository;
import com.allu.spring.repositories.StudentRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.stubbing.answers.DoesNothing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@WebMvcTest
public class StudentRestControllerMvcTest {

    private static final String STUDENTS_URL = "/students";


    private static final int STUDENT_ID = 1;
    private static final String STUDENT_NAME = "ALLU";
    private static final int STUDENT_TESTSCORE = 99;

    @MockBean
    private StudentRepository repository;

    @MockBean
    private CourseRepository courseRepository;
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testGetAllStudents() throws Exception {
        Student student = buildStudent();

        List <Student> students = Arrays.asList(student);
        when(repository.findAll()).thenReturn(students);
        ObjectWriter objectWriter =new ObjectMapper().writer().withDefaultPrettyPrinter();

        mockMvc.perform(get(STUDENTS_URL)).andExpect(status().isOk())
               // .andExpect(content().json("[{'id':1,'name': 'ARJUN','testscore':'999'}]"));
        .andExpect(content().json(objectWriter.writeValueAsString(students)));
    }


    @Test
    public void testCreateStudent() throws Exception {
        Student student = buildStudent();

        when(repository.save(any())).thenReturn(student);

        ObjectWriter objectWriter =new ObjectMapper().writer().withDefaultPrettyPrinter();

        mockMvc.perform(post(STUDENTS_URL).contextPath("").contentType(MediaType.APPLICATION_JSON)
                        .content(objectWriter.writeValueAsString(student)))
                         .andExpect(status().isOk())
                         .andExpect(content().json(objectWriter.writeValueAsString(student)))
        ;
    }

    @Test
    public void testUpdateStudent() throws Exception {
        Student student = buildStudent();

        when(repository.save(any())).thenReturn(student);

        ObjectWriter objectWriter =new ObjectMapper().writer().withDefaultPrettyPrinter();

        mockMvc.perform(put(STUDENTS_URL).contextPath("").contentType(MediaType.APPLICATION_JSON)
                .content(objectWriter.writeValueAsString(student)))
                .andExpect(status().isOk())
                .andExpect(content().json(objectWriter.writeValueAsString(student)))
        ;
    }

    @Test
    public void testDeleteStudent() throws Exception {
        doNothing().when(repository).deleteById((long) 1);

        mockMvc.perform(delete(STUDENTS_URL+"/2").contextPath("")).andExpect(status().isOk())
                .andExpect(content().string("row is deleted"));
    }

    private Student buildStudent() {
        Student student =new Student();
        student.setId(STUDENT_ID);
        student.setName(STUDENT_NAME);
        student.setTestscore(STUDENT_TESTSCORE);
        return student;
    }
}
