package com.allu.spring;

import com.allu.spring.entities.Student;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StudentRestapiApplicationTests {

	@Value("${studentrestapi.services.baseUrl}")
	private String baseUrl;

	RestTemplate temp = new RestTemplate();

	@Test
	public void testGetStudent() {
		System.out.println(baseUrl);
		Student stu=temp.getForObject(baseUrl +"1", Student.class);
		assertNotNull(stu);
		assertEquals(stu.getName(),"ALLU SAI PRUDHVI");
	}

	@Test
	public void testPostStudent() {
		Student stu=new Student();
		stu.setId(2);
		stu.setName("SAI RAM");
		stu.setTestscore(200);
		Student test_stu=temp.postForObject(baseUrl,stu, Student.class);
		assertNotNull(test_stu);
		assertEquals(test_stu.getName(),stu.getName());
	}

	@Test
	public void testUpdateStudent() {
		Student stu=new Student();
		stu.setId(2);
		stu.setName("JAI SAI RAM");
		stu.setTestscore(1000);
		temp.put("http://localhost:8080/students",stu);
		Student test_stu=temp.getForObject(baseUrl+"2", Student.class);
		assertNotNull(test_stu);
		assertEquals(test_stu.getTestscore(),stu.getTestscore());
	}
}
