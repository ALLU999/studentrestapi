package com.allu.spring.repositories;

import com.allu.spring.entities.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

public interface StudentRepository extends CrudRepository<Student, Long> {
}
