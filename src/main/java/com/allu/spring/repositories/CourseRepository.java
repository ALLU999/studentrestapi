package com.allu.spring.repositories;

import com.allu.spring.entities.Course;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

public interface CourseRepository extends CrudRepository<Course,String> {


}
