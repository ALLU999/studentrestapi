package com.allu.spring.services;

import com.allu.spring.daos.EnrollementDao;
import com.allu.spring.daos.EnrollementDaoImpl;
import com.allu.spring.dtos.responses.CoursesEnrolledResponseDto;
import com.allu.spring.entities.Course;
import com.allu.spring.errorResponse.MyProjectException;
import com.allu.spring.dtos.requests.EnrollmentRequestDto;
import com.allu.spring.entities.Student;
import com.allu.spring.repositories.CourseRepository;
import com.allu.spring.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class EnrollmentServiceImpl implements EnrollmentService{

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private EnrollementDao enrollementDao;

    @Override
    public void enroll(EnrollmentRequestDto request) throws MyProjectException {
        long student_id = request.getStudent_id();
        Optional<Student> student =studentRepository.findById(student_id);
        if(!student.isPresent())
            throw new MyProjectException("Student Id doesn't exist");

        String course_id = request.getCourse_id();
        Optional<Course> course=courseRepository.findById(course_id);
        if(!course.isPresent())
            throw new MyProjectException("Course Id doesn't exist");

        if(enrollementDao.checkIfStudentEnrolledCourse(request))
            throw new MyProjectException("Student with id :" + request.getStudent_id()+" has already enrolled for this course with id : "+request.getCourse_id());

        enrollementDao.enroll(request);
    }

    @Override
    public List<CoursesEnrolledResponseDto> getEnrolledCoursesByStudent(long student_id) {
        return enrollementDao.getEnrolledCoursesByStudent(student_id);
    }
}
