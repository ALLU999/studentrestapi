package com.allu.spring.services;

import com.allu.spring.dtos.responses.CoursesEnrolledResponseDto;
import com.allu.spring.errorResponse.MyProjectException;
import com.allu.spring.dtos.requests.EnrollmentRequestDto;

import java.util.List;

public interface EnrollmentService {

    public void enroll(EnrollmentRequestDto request) throws MyProjectException;

    public List<CoursesEnrolledResponseDto> getEnrolledCoursesByStudent(long student_id);
}
