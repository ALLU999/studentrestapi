package com.allu.spring.Controllers;

import com.allu.spring.errorResponse.MyProjectException;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
@RequestMapping("/files")
public class FileController {

    @PostMapping("/upload")
    public Object uploadFile(@RequestParam("file") MultipartFile file) throws MyProjectException {
        try {
            File new_file = new File("C:\\Users\\Lenovo\\Desktop\\"+file.getOriginalFilename());
            System.out.println(new_file.getAbsolutePath());
            file.transferTo(new_file);
            return true;
        }
        catch (IOException err){
            throw new MyProjectException(err);
        }
    }

    @GetMapping("/download/{fileName}")
    public ResponseEntity <byte[]> download( @PathVariable("fileName") String fileName) throws IOException {
        byte[] fileData=Files.readAllBytes(new File("C:\\Users\\Lenovo\\Desktop"+fileName).toPath());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.IMAGE_JPEG);
        return new ResponseEntity<byte[]>(fileData,headers, HttpStatus.OK);
    }
}
