package com.allu.spring.Controllers;

import com.allu.spring.entities.Course;
import com.allu.spring.repositories.CourseRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RequestMapping("/courses")
@RestController
public class CourseController {
    @Autowired
    CourseRepository repository;

    private static final Logger LOGGER =  LoggerFactory.getLogger(StudentController.class);


    @ApiOperation(value="Retrieve all the courses",
            notes = "A list of courses",
            response = Course.class ,
            responseContainer = "List",
            produces = "application/json"
    )
    @GetMapping
    List<Course> getAllCourses(){
        LOGGER.info("Fetching all students data :  ");

        return (List<Course>) repository.findAll();
    }

    @ApiOperation(value=" Course Details By Id",
            notes = "Course details",
            response = Course.class ,
            produces = "application/json"
    )
    @GetMapping("/{id}")
    @Transactional(readOnly = true)
    Optional<Course> getCourseById(@ApiParam(value="course id that needs details",required = true) @PathVariable("id") String id){
        LOGGER.info("Finding course By Id :  "+id);
        return repository.findById(id);
    }

    @ApiOperation(value="Add Course to Database ",
            notes = "Adding Course",
            response = Course.class ,
            produces = "application/json"
    )
    @PostMapping
    Course saveCourse(@Valid @RequestBody Course c){
        LOGGER.info("Saving Course Details:  "+c);
        return repository.save(c);
    }

    @ApiOperation(value="update Course  data in Database ",
            notes = "updating Course Details",
            response = Course.class ,
            produces = "application/json"
    )
    @PutMapping
    Course updateCourse(@ApiParam(value = " Course Json Object" , required = true) @RequestBody Course c){
        LOGGER.info("Updating Student  Details :  "+c);
        return repository.save(c);
    }

    @ApiOperation(value="Delete Course  data in Database ",
            notes = "Deleting course",
            response = String.class ,
            produces = "text/plain"
    )
    @DeleteMapping("/{id}")
    String deleteCourse(@ApiParam(value="student id that should be deleted",required = true) @PathVariable("id") String id){
        try{
            LOGGER.info("Delete Student  Details :  "+id);
            repository.deleteById(id);
            return "row is deleted";
        }catch(Exception e){
            return e.getMessage();
        }
    }
}
