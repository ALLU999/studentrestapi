package com.allu.spring.Controllers;

import com.allu.spring.entities.Student;
import com.allu.spring.repositories.StudentRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    StudentRepository repository;

    private static final Logger LOGGER =  LoggerFactory.getLogger(StudentController.class);


    @ApiOperation(value="Retrieve all the students",
            notes = "A list of students",
            response = Student.class ,
            responseContainer = "List",
            produces = "application/json"
    )
    @GetMapping
    List<Student> getAllStudents(){
        LOGGER.info("Fetching all students data :  ");

        return (List<Student>) repository.findAll();
    }

    @ApiOperation(value=" Student Details By Id",
            notes = "Student details",
            response = Student.class ,
            produces = "application/json"
    )
    @GetMapping("/{id}")
    @Transactional(readOnly = true)
    @Cacheable("student-cache")
    Optional<Student> getStudentById(@ApiParam(value="student id that needs details",required = true) @PathVariable("id") Long id){
        LOGGER.info("Finding product By Id :  "+id);
        return repository.findById(id);
    }

    @ApiOperation(value="Add Student to Database ",
            notes = "Adding Student",
            response = Student.class ,
            produces = "application/json"
    )
    @PostMapping
    Student saveStudent(@Valid @RequestBody Student s){
        LOGGER.info("Saving Student Details:  "+s);
        return repository.save(s);
    }

    @ApiOperation(value="update Student  data in Database ",
            notes = "updating Student Details",
            response = Student.class ,
            produces = "application/json"
    )
    @PutMapping
    Student updateStudent(@ApiParam(value = " Student Json Object" , required = true) @RequestBody Student s){
        LOGGER.info("Updating Student  Details :  "+s);
        return repository.save(s);
    }

    @ApiOperation(value="Delete Student  data in Database ",
            notes = "Deleting Student",
            response = String.class ,
            produces = "text/plain"
    )
    @DeleteMapping("/{id}")
    @CacheEvict("student-cache")
    String deleteStudent(@ApiParam(value="student id that should be deleted",required = true) @PathVariable("id") Long id){
        try{
            LOGGER.info("Delete Student  Details :  "+id);
            repository.deleteById(id);
            return "row is deleted";
        }catch(Exception e){
            return e.getMessage();
        }
    }
}
