package com.allu.spring.Controllers;

import com.allu.spring.dtos.responses.CoursesEnrolledResponseDto;
import com.allu.spring.entities.Student;
import com.allu.spring.errorResponse.MyProjectException;
import com.allu.spring.dtos.requests.EnrollmentRequestDto;
import com.allu.spring.services.EnrollmentServiceImpl;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.List;

@RequestMapping("/course-enrollment")
@RestController
public class EnrollmentController {

    @Autowired(required=false)
    private EnrollmentServiceImpl service;

    @ApiOperation(value="Enroll student to a course ",
            notes = "enroll",
            response = String.class ,
            produces = "text/plain"
    )
    @PostMapping("/enroll")
    public ResponseEntity<String> enroll(@RequestBody EnrollmentRequestDto requestDto) throws MyProjectException {
        System.out.println(requestDto);
        service.enroll(requestDto);
        return new ResponseEntity<String>("Enrolled Successfully", HttpStatus.OK);
    }


    @ApiOperation(value="Get the list of the courses and enrolled time enrolled by the student",
            notes = "A list of courses",
            response = CoursesEnrolledResponseDto.class ,
            responseContainer = "List",
            produces = "application/json"
    )
    @GetMapping("/student-enrolled-courses/{student_id}")
    public ResponseEntity<List<CoursesEnrolledResponseDto>> enrolledCoursesByStudent(@ApiParam(value="student id that needs details",required = true) @PathVariable("student_id") long student_id) throws MyProjectException{
        List<CoursesEnrolledResponseDto> courses=service.getEnrolledCoursesByStudent(student_id);
        return new ResponseEntity<List<CoursesEnrolledResponseDto>>(courses,HttpStatus.OK);
    }
}
