package com.allu.spring.daos;

import com.allu.spring.dtos.requests.EnrollmentRequestDto;
import com.allu.spring.dtos.responses.CoursesEnrolledResponseDto;

import java.util.List;

public interface EnrollementDao {
    public void enroll(EnrollmentRequestDto request);

    public boolean checkIfStudentEnrolledCourse(EnrollmentRequestDto request);

    public List<CoursesEnrolledResponseDto> getEnrolledCoursesByStudent(long student_id);
}
