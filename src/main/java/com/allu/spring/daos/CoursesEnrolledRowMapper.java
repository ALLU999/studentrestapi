package com.allu.spring.daos;

import com.allu.spring.dtos.responses.CoursesEnrolledResponseDto;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;


public class CoursesEnrolledRowMapper implements RowMapper<CoursesEnrolledResponseDto> {

    @Override
    public CoursesEnrolledResponseDto mapRow(ResultSet rs, int rowNum) throws SQLException {
        CoursesEnrolledResponseDto responseDto = new CoursesEnrolledResponseDto();
        responseDto.setCourse_id(rs.getString("course_id"));
        responseDto.setCourse_name(rs.getString("course_name"));
        responseDto.setFaculty_name(rs.getString("faculty_name"));
        responseDto.setEnrolled_time(rs.getString("enrolled_time"));
        return responseDto;
    }
}
