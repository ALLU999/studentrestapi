package com.allu.spring.daos;

import com.allu.spring.dtos.requests.EnrollmentRequestDto;
import com.allu.spring.dtos.responses.CoursesEnrolledResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Component
public class EnrollementDaoImpl implements EnrollementDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public void enroll(EnrollmentRequestDto request) {
        String sql="insert into enroll values(?,?,?)";
        jdbcTemplate.update(sql,request.getStudent_id(),request.getCourse_id(),request.getEnrolled_time());
    }

    @Override
    public boolean checkIfStudentEnrolledCourse(EnrollmentRequestDto request) {

        String sql = "select * from enroll where student_id=? and course_id=?";
        List<Map<String, Object>> result=jdbcTemplate.queryForList(sql , request.getStudent_id(),request.getCourse_id());
        System.out.println(result);
        if(result.size() > 0)
            return true;
        return false;
    }

    @Override
    public List<CoursesEnrolledResponseDto> getEnrolledCoursesByStudent(long student_id) {
        String sql ="select course_id, name as course_name , faculty_name , enrolled_time from (select * from enroll where student_id=?) as t1 inner join course on course.id=t1.course_id";
        CoursesEnrolledRowMapper rowMapper = new CoursesEnrolledRowMapper();
        List<CoursesEnrolledResponseDto> result=jdbcTemplate.query(sql , new Object[]{student_id},rowMapper );
        return result;
    }


}
