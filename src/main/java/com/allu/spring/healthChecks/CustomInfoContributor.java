package com.allu.spring.healthChecks;

import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class CustomInfoContributor implements InfoContributor {
    @Override
    public void contribute(Info.Builder builder) {

        Map<String, String> collegeDetails = new HashMap<>();
        collegeDetails.put("College Name","NITC" );
        collegeDetails.put("Address","Kerala" );
        builder.withDetail("College info" , collegeDetails);
    }
}
