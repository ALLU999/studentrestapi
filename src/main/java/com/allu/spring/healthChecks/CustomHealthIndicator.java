package com.allu.spring.healthChecks;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class CustomHealthIndicator implements HealthIndicator {

    @Override
    public Health health() {

        Boolean error =false ;

        if(error)
            return Health.down().withDetail("Error key", 123).build();

        return Health.up().build();
    }
}
