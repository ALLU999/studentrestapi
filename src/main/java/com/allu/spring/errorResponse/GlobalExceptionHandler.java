package com.allu.spring.errorResponse;

import com.allu.spring.dtos.errors.ErrorDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.Date;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(MyProjectException.class)
    public ResponseEntity<ErrorDto> myProjectException( MyProjectException ex){
        ErrorDto errorDto = new ErrorDto();
        errorDto.setMessage(ex.getMessage());
        errorDto.setStatus(String.valueOf(ex.getStatus().value()));
        errorDto.setTime(new Date().toString());

        return new ResponseEntity<ErrorDto>(errorDto,ex.getStatus());
    }
}
