package com.allu.spring.errorResponse;

import org.springframework.http.HttpStatus;

public class MyProjectException extends Exception  {

   public MyProjectException(String s){
        super(s);
    }

   public MyProjectException(String s ,Throwable err ){
        super(s,err);
    }

    public MyProjectException(Throwable err ){
       super(err);
    }

    public HttpStatus getStatus() {
        return HttpStatus.BAD_REQUEST;
    }
}
