package com.allu.spring.dtos.responses;

public class CoursesEnrolledResponseDto {

    private String course_id;
    private String course_name;
    private String faculty_name;
    private String enrolled_time;

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getFaculty_name() {
        return faculty_name;
    }

    public void setFaculty_name(String faculty_name) {
        this.faculty_name = faculty_name;
    }

    public String getEnrolled_time() {
        return enrolled_time;
    }

    public void setEnrolled_time(String enrolled_time) {
        this.enrolled_time = enrolled_time;
    }

    @Override
    public String toString() {
        return "CoursesEnrolledResponseDto{" +
                "course_id='" + course_id + '\'' +
                ", course_name='" + course_name + '\'' +
                ", faculty_name='" + faculty_name + '\'' +
                ", enrolled_time='" + enrolled_time + '\'' +
                '}';
    }
}
