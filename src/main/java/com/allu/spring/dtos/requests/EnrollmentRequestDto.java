package com.allu.spring.dtos.requests;

import java.sql.Timestamp;

public class EnrollmentRequestDto {
    private String course_id;
    private long student_id;
    private Timestamp enrolled_time;

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public long getStudent_id() {
        return student_id;
    }

    public void setStudent_id(long student_id) {
        this.student_id = student_id;
    }

    public Timestamp getEnrolled_time() {
        return enrolled_time;
    }

    public void setEnrolled_time(Timestamp enrolled_time) {
        this.enrolled_time = enrolled_time;
    }

    @Override
    public String toString() {
        return "EnrollmentRequestDto{" +
                "course_id='" + course_id + '\'' +
                ", student_id=" + student_id +
                ", enrolled_time=" + enrolled_time +
                '}';
    }
}
